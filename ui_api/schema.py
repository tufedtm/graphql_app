import graphene
from graphene_django.debug import DjangoDebug

from main import mutations, schema


class Query(schema.Query, graphene.ObjectType):
    debug = graphene.Field(DjangoDebug, name='_debug')


class Mutation(mutations.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)
