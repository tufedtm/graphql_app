FROM python

ENV PYTHONUNBUFFERED 1

RUN pip install -U pip setuptools django django-filter dj-database-url factory_boy graphene_django psycopg2-binary
