import factory

from .models import Applicant, Company, Currency, Customer, CustomerRequirement, Lot, Notification, Region


class CurrencyFactory(factory.DjangoModelFactory):
    code, name = factory.Faker('currency')

    class Meta:
        model = Currency


class RegionFactory(factory.DjangoModelFactory):
    name = factory.Faker('country')
    kladr_code = factory.Faker('country_code')

    class Meta:
        model = Region


class CustomerFactory(factory.DjangoModelFactory):
    reg_number = factory.Faker('uuid4')
    cons_registry_num = factory.Faker('uuid4')
    full_name = factory.Faker('company')

    class Meta:
        model = Customer


class CompanyFactory(factory.DjangoModelFactory):
    full_name = factory.Faker('company')

    class Meta:
        model = Company


class NotificationFactory(factory.DjangoModelFactory):
    purchase_number = factory.Faker('uuid4')
    filing_date_start = factory.Faker('date_time')
    filing_date_end = factory.Faker('date_time')
    auction_date = factory.Faker('date_time')
    stage = factory.Faker('random_element', elements=[x[0] for x in Notification.STAGE])
    law = factory.Faker('random_element', elements=[x[0] for x in Notification.LAW])
    placing_way = factory.Faker('random_element', elements=[x[0] for x in Notification.PLACING_WAY])
    purchase_object_info = factory.Faker('text')
    responsible_role = factory.Faker('random_element', elements=[x[0] for x in Notification.RESPONSIBLE_ROLE])
    region = factory.Iterator(Region.objects.all())
    responsible_org = factory.Iterator(Customer.objects.all())

    class Meta:
        model = Notification


class LotFactory(factory.DjangoModelFactory):
    notification = factory.Iterator(Notification.objects.all())
    currency = factory.Iterator(Currency.objects.all())
    lot_object_info = factory.Faker('text')
    max_price = factory.Faker('pydecimal', positive=True)

    class Meta:
        model = Lot


class ApplicantFactory(factory.DjangoModelFactory):
    lot = factory.Iterator(Lot.objects.all())
    applicant = factory.Iterator(Company.objects.all())
    applicant_stage = factory.Faker('random_element', elements=[x[0] for x in Applicant.APPLICANT_STAGE])

    class Meta:
        model = Applicant


class CustomerRequirementFactory(factory.DjangoModelFactory):
    lot = factory.Iterator(Lot.objects.all())
    customer = factory.Iterator(Customer.objects.all())
    max_price = factory.Faker('pydecimal', positive=True)
    application_guarantee_amount = factory.Faker('pydecimal', positive=True)
    contract_guarantee_amount = factory.Faker('pydecimal', positive=True)
    delivery_term = factory.Faker('text')

    class Meta:
        model = CustomerRequirement


with factory.Faker.override_default_locale('ru_RU'):
    for _ in range(10):
        CurrencyFactory.create()
    for _ in range(100):
        RegionFactory.create()
    for _ in range(250):
        CustomerFactory.create()
    for _ in range(250):
        CompanyFactory.create()
    for _ in range(1000):
        NotificationFactory.create()
    for _ in range(1500):
        LotFactory.create()
    for _ in range(2000):
        ApplicantFactory.create()
    for _ in range(2000):
        CustomerRequirementFactory.create()
