from graphene_django.forms.mutation import DjangoModelFormMutation

from .forms import CompanyForm


class CompanyMutation(DjangoModelFormMutation):
    class Meta:
        form_class = CompanyForm


class Mutation:
    company = CompanyMutation.Field()
