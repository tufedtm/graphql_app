from django.db import models


class BaseModel(models.Model):
    archived = models.BooleanField('В архиве', default=False)
    created_at = models.DateTimeField('Дата создания', auto_now_add=True)
    updated_at = models.DateTimeField('Дата обновления', auto_now=True)

    class Meta:
        abstract = True


class Platform(BaseModel):
    pass


class Currency(BaseModel):
    code = models.CharField('Код валюты', max_length=3)
    name = models.CharField('Наименование валюты', max_length=50)

    class Meta:
        verbose_name = 'Валюта'
        verbose_name_plural = 'Валюты'


class Region(BaseModel):
    name = models.CharField('Наименование', max_length=255)
    kladr_code = models.CharField('Код КЛАДР', max_length=2)

    class Meta:
        verbose_name = 'Регион РФ'
        verbose_name_plural = 'Регионы РФ'


class Customer(BaseModel):
    reg_number = models.CharField('Реестровый номер в СПЗ', max_length=255)
    cons_registry_num = models.CharField('Код по Сводному Реестру', max_length=255)
    full_name = models.CharField('Полное наименование', max_length=2000)

    class Meta:
        verbose_name = 'Заказчик'
        verbose_name_plural = 'Заказчики'


class Company(BaseModel):
    full_name = models.CharField('Полное наименование', max_length=2000)

    class Meta:
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'


class Notification(BaseModel):
    LAW = (
        (0, '94-ФЗ'),
        (1, '44-ФЗ'),
        (2, '223-ФЗ'),
        (3, 'ПП РФ 615'),
    )
    PLACING_WAY = (
        (1, 'Электронный аукцион'),
        (2, 'Открытый конкурс'),
        (3, 'Конкурс c ограниченным участием'),
        (4, 'Открытый конкурс в электронной форме'),
        (5, 'Запросы предложений'),
        (6, 'Запросы предложений в электронной форме'),
    )
    RESPONSIBLE_ROLE = (
        ('CU', 'Заказчик'),
        ('OCU', 'Заказчик в качестве организатора совместного аукциона'),
        ('RA', 'Уполномоченный орган'),
        ('ORA', 'Уполномоченный орган в качестве организатора совместного конкурса (аукциона) согласно ст. 25 №44ФЗ'),
        ('AI', 'Уполномоченное учреждение'),
        ('OAI', 'Уполномоченное учреждение '
                'в качестве организатора совместного конкурса (аукциона) согласно ст. 25 №44ФЗ'),
        ('OA', 'Организация, осуществляющая полномочия заказчика на осуществление закупок '
               'на основании договора (соглашения)'),
        ('OOA', 'Организация, осуществляющая полномочия заказчика на осуществление закупок '
                'на основании договора (соглашения) в качестве организатора совместного конкурса (аукциона) '
                'согласно ст. 25 №44ФЗ'),
        ('CS', 'Заказчик, осуществляющий закупки в соответствии с частью 5 статьи 15 Федерального закона № 44-ФЗ'),
        ('OCS', 'Заказчик, осуществляющий закупки в соответствии с частью 5 статьи 15 Федерального закона № 44-ФЗ, '
                'в качестве организатора совместного конкурса (аукциона) согласно ст. 25 №44ФЗ'),
        ('CC', 'Заказчик, осуществляющий закупки в соответствии с Федеральным законом № 44-ФЗ, '
               'в связи с неразмещением положения о закупке в соответствии с положениями Федерального закона № 223-ФЗ'),
        ('OCC', 'Заказчик, осуществляющий закупки в соответствии с Федеральным законом № 44-ФЗ, '
                'в связи с неразмещением положения о закупке в соответствии с положениями Федерального закона № 223-ФЗ, '
                'в качестве организатора совместного конкурса (аукциона) согласно ст. 25 №44ФЗ'),
        ('AU', 'Заказчик, осуществляющий закупку на проведение обязательного аудита (код AU)'),
        ('OAU', 'Заказчик, осуществляющий закупку на проведение обязательного аудита (код AU), '
                'в качестве организатора совместного конкурса (аукциона) согласно ст. 25 №44ФЗ'),
        ('RO', 'Региональный оператор'),
        ('TKO', 'Региональный оператор для обращения с ТБО'),
    )
    STAGE = (
        (0, 'Процедура отменена'),
        (1, 'Подача заявок'),
        (2, 'Работа комиссии'),
        (3, 'Процедура завершена'),
    )

    purchase_number = models.CharField('Номер закупки', max_length=255, unique=True)
    filing_date_start = models.DateTimeField('Дата начала подачи заявок')
    filing_date_end = models.DateTimeField('Дата конца подачи заявок')
    auction_date = models.DateTimeField(
        'Дата проведения аукциона',
        null=True,
        blank=True,
        help_text='только для ЭА 44ФЗ',
    )
    stage = models.PositiveSmallIntegerField('Этап закупки', choices=STAGE, default=1)
    law = models.PositiveSmallIntegerField('ФЗ', choices=LAW, default=1)
    region = models.ForeignKey(Region, on_delete=models.DO_NOTHING, verbose_name='Субъект РФ заказчика')
    placing_way = models.PositiveSmallIntegerField(
        'Способ определения поставщика, подрядной организации (размещения закупки)',
        choices=PLACING_WAY,
        default=1,
    )
    purchase_object_info = models.TextField('Наименование объекта закупки')
    responsible_org = models.ForeignKey(
        Customer,
        on_delete=models.DO_NOTHING,
        verbose_name='Организация, осуществляющая закупку',
    )
    responsible_role = models.CharField(
        'Роль организации, осуществляющей закупку',
        choices=RESPONSIBLE_ROLE,
        max_length=3,
    )

    class Meta:
        verbose_name = 'Извещение'
        verbose_name_plural = 'Извещения'


class Lot(BaseModel):
    notification = models.ForeignKey(Notification, on_delete=models.DO_NOTHING, verbose_name='Извещение')
    lot_object_info = models.TextField('Наименование объекта закупки для лота')
    max_price = models.DecimalField('НМЦК', max_digits=21, decimal_places=2)
    currency = models.ForeignKey(Currency, on_delete=models.DO_NOTHING, verbose_name='Валюта')
    suppliers = models.ManyToManyField(
        Company,
        verbose_name='Поставщик',
        through='Applicant',
    )

    class Meta:
        verbose_name = 'Лот извещения'
        verbose_name_plural = 'Лоты извещения'


class Applicant(BaseModel):
    APPLICANT_STAGE = (
        (1, 'Заявитель'),
        (2, 'Участник'),
        (3, 'Победитель'),
    )
    lot = models.ForeignKey(Lot, on_delete=models.DO_NOTHING, verbose_name='Лот извещения')
    applicant = models.ForeignKey(Company, on_delete=models.DO_NOTHING, verbose_name='Заявитель')
    applicant_stage = models.PositiveSmallIntegerField('Статус участия заявителя', choices=APPLICANT_STAGE, default=1)

    class Meta:
        verbose_name = 'Заявитель'
        verbose_name_plural = 'Заявители'


class CustomerRequirement(BaseModel):
    lot = models.ForeignKey(Lot, on_delete=models.DO_NOTHING, verbose_name='Лот извещения')
    customer = models.ForeignKey(
        Customer,
        on_delete=models.DO_NOTHING,
        verbose_name='Организация заказчика данных требований',
    )
    max_price = models.DecimalField('НМЦК', max_digits=21, decimal_places=2)
    application_guarantee_amount = models.DecimalField('ОЗ', max_digits=21, decimal_places=2)
    contract_guarantee_amount = models.DecimalField('ОИК', max_digits=21, decimal_places=2)
    delivery_term = models.TextField(
        'Сроки доставки товара, выполнения работы или оказания услуги либо график оказания услуг',
    )

    class Meta:
        verbose_name = 'Требование заказчика'
        verbose_name_plural = 'Требования заказчиков'
