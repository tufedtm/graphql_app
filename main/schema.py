import graphene
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField
from graphene_django.types import DjangoObjectType

from .models import Applicant, Company, Currency, Customer, CustomerRequirement, Lot, Notification, Region


class ApplicantType(DjangoObjectType):
    applicant_stage = graphene.JSONString()

    def resolve_applicant_stage(self, info, **kwargs):
        return {
            'code': self.applicant_stage,
            'data': self.get_applicant_stage_display()
        }

    class Meta:
        model = Applicant


class CompanyType(DjangoObjectType):
    class Meta:
        model = Company
        filter_fields = ['full_name']
        interfaces = (relay.Node,)


class CurrencyType(DjangoObjectType):
    class Meta:
        model = Currency
        filter_fields = {
            'code': ['exact', 'istartswith'],
            'name': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)


class CustomerType(DjangoObjectType):
    class Meta:
        model = Customer
        filter_fields = {
            'reg_number': ['exact'],
            'cons_registry_num': ['exact'],
            'full_name': ['exact', 'icontains', 'istartswith'],
        }
        interfaces = (relay.Node,)


class CustomerRequirementType(DjangoObjectType):
    class Meta:
        model = CustomerRequirement


class LotType(DjangoObjectType):
    class Meta:
        model = Lot
        filter_fields = ['notification', 'max_price', 'currency', 'suppliers']
        interfaces = (relay.Node,)


class NotificationType(DjangoObjectType):
    law = graphene.JSONString()
    placing_way = graphene.JSONString()
    responsible_role = graphene.JSONString()
    stage = graphene.JSONString()

    def resolve_law(self, info, **kwargs):
        return {
            'code': self.law,
            'data': self.get_law_display()
        }

    def resolve_placing_way(self, info, **kwargs):
        return {
            'code': self.placing_way,
            'data': self.get_placing_way_display()
        }

    def resolve_responsible_role(self, info, **kwargs):
        return {
            'code': self.responsible_role,
            'data': self.get_responsible_role_display()
        }

    def resolve_stage(self, info, **kwargs):
        return {
            'code': self.stage,
            'data': self.get_stage_display()
        }

    class Meta:
        model = Notification
        filter_fields = [
            'purchase_number', 'filing_date_start', 'filing_date_end', 'auction_date', 'stage', 'law', 'region',
            'placing_way', 'purchase_object_info', 'responsible_org', 'responsible_role'
        ]
        interfaces = (relay.Node,)


class RegionType(DjangoObjectType):
    class Meta:
        model = Region
        filter_fields = ['name', 'kladr_code']
        interfaces = (relay.Node,)


class Query:
    company = relay.Node.Field(CompanyType)
    companies = DjangoFilterConnectionField(CompanyType)
    currency = relay.Node.Field(CurrencyType)
    currencies = DjangoFilterConnectionField(CurrencyType)
    customer = relay.Node.Field(CustomerType)
    customers = DjangoFilterConnectionField(CustomerType)
    lot = relay.Node.Field(LotType)
    lots = DjangoFilterConnectionField(LotType)
    notification = relay.Node.Field(NotificationType)
    notifications = DjangoFilterConnectionField(NotificationType)
    region = relay.Node.Field(RegionType)
    regions = DjangoFilterConnectionField(RegionType)
