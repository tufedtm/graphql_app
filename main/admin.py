from django.contrib import admin

from .models import Applicant, Company, Currency, Customer, CustomerRequirement, Lot, Notification, Platform, Region

admin.site.register((Applicant, CustomerRequirement, Notification, Platform))


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('code', 'name')


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('reg_number', 'cons_registry_num', 'full_name')


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('full_name',)


@admin.register(Region)
class RegionAdmin(admin.ModelAdmin):
    list_display = ('name', 'kladr_code')


class ApplicantInline(admin.TabularInline):
    model = Applicant
    extra = 1


@admin.register(Lot)
class LotAdmin(admin.ModelAdmin):
    inlines = (ApplicantInline,)
